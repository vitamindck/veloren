stages:
  - optional-builds
  - check-compile
  - post-build

# Our own git fetch command like https://gitlab.com/gitlab-org/gitlab-runner/blob/master/shells/abstract.go
# speeds up building because we skip the git clean and don't need any gitlab caches.
variables:
  GIT_STRATEGY: none
before_script:
  - mkdir -p /cache/veloren
  - rm -rf /cache/veloren/bin
  - cd /cache/veloren
  - if [ -d .git ]; then
       echo "is git dir";
    else
      git clone $CI_REPOSITORY_URL . ;
    fi;
  - mkdir -p /cache/veloren/bin
  - rm -f .git/index.lock
  - rm -f .git/shallow.lock
  - rm -f .git/HEAD.lock
  - rm -f .git/hocks/post-checkout
  - git remote set-url origin $CI_REPOSITORY_URL
  - git fetch origin --prune +refs/heads/*:refs/remotes/origin/* +refs/tags/*:refs/tags/*
  - git checkout -f -q $CI_COMMIT_SHA
  - if [ ! -z "${SOURCE_PROJECT}" -a "${SOURCE_PROJECT}" != " " ]; then
      echo "THIS SEEMS TO BE A MERGE PIPELINE FROM ${SOURCE_PROJECT}/${SOURCE_BRANCH}";
      git pull "https://gitlab.com/${SOURCE_PROJECT}/veloren.git" "${SOURCE_BRANCH}";
    fi;
  - git lfs install;
  - git lfs fetch;
  - git lfs checkout;
  - git status
  - rustup -v show
  - if [ -d target ]; then
      ls -la target;
    fi;

#############
# Optional build
#############

.optional: &optional
  stage: optional-builds
  image: registry.gitlab.com/veloren/veloren-docker-ci
  tags:
    - veloren-docker
  except:
    - schedules
  when: manual
  artifacts:
    paths:
      - ./
    expire_in: 1 week

optional:linux-debug:
  <<: *optional
  script:
    - (cd voxygen && VELOREN_ASSETS=assets cargo build)
    - (cd server-cli && VELOREN_ASSETS=assets cargo build)
    - cp target/debug/veloren-server-cli $CI_PROJECT_DIR
    - cp target/debug/veloren-voxygen $CI_PROJECT_DIR
    - cp -r assets $CI_PROJECT_DIR

optional:windows-debug:
  <<: *optional
  script:
    - (cd voxygen && VELOREN_ASSETS=assets cargo build --target=x86_64-pc-windows-gnu)
    - (cd server-cli && VELOREN_ASSETS=assets cargo build --target=x86_64-pc-windows-gnu)
    - cp target/x86_64-pc-windows-gnu/debug/veloren-server-cli.exe $CI_PROJECT_DIR
    - cp target/x86_64-pc-windows-gnu/debug/veloren-voxygen.exe $CI_PROJECT_DIR
    - cp -r assets $CI_PROJECT_DIR

#############
# Optional release build
#############

.optional-release: &optional-release
  stage: optional-builds
  image: registry.gitlab.com/veloren/veloren-docker-ci
  tags:
    - veloren-docker
  except:
    - schedules
  when: manual
  artifacts:
    paths:
      - ./
    expire_in: 1 month

optional-release:linux-debug:
  <<: *optional-release
  script:
    - (cd voxygen && VELOREN_ASSETS=assets cargo build --release)
    - (cd server-cli && VELOREN_ASSETS=assets cargo build --release)
    - cp target/debug/veloren-server-cli $CI_PROJECT_DIR
    - cp target/debug/veloren-voxygen $CI_PROJECT_DIR
    - cp -r assets $CI_PROJECT_DIR

optional-release:windows-debug:
  <<: *optional-release
  script:
    - (cd voxygen && VELOREN_ASSETS=assets cargo build --release --target=x86_64-pc-windows-gnu)
    - (cd server-cli && VELOREN_ASSETS=assets cargo build --release --target=x86_64-pc-windows-gnu)
    - cp target/x86_64-pc-windows-gnu/debug/veloren-server-cli.exe $CI_PROJECT_DIR
    - cp target/x86_64-pc-windows-gnu/debug/veloren-voxygen.exe $CI_PROJECT_DIR
    - cp -r assets $CI_PROJECT_DIR

#############
# CHECK
#############

check-voxygen:
  stage: check-compile
  image: registry.gitlab.com/veloren/veloren-docker-ci
  tags:
    - veloren-docker
  script:
    - (cd voxygen && RUSTFLAGS="-D warnings" cargo check)

check-server-cli:
  stage: check-compile
  image: registry.gitlab.com/veloren/veloren-docker-ci
  tags:
    - veloren-docker
  script:
    - (cd server-cli && RUSTFLAGS="-D warnings" cargo check)

#############
# POST-BUILD
#############

unittests:
  stage: post-build
  image: registry.gitlab.com/veloren/veloren-docker-ci
  tags:
    - veloren-docker
  script:
    - cargo test

benchmarktests:
  stage: post-build
  image: registry.gitlab.com/veloren/veloren-docker-ci
  tags:
    - veloren-docker
  script:
    - cargo bench
  allow_failure: true

clean-code:
  stage: post-build
  image: registry.gitlab.com/veloren/veloren-docker-ci
  tags:
    - veloren-docker
  script:
    - cargo fmt --all -- --check

# # Coverage needs to be disabled until an issue in the Rust compiler is fixed
# # https://github.com/rust-lang/rust/issues/58375
# coverage:
#   stage: post-build
#   image: registry.gitlab.com/veloren/veloren-docker-ci
#   tags:
#     - veloren-docker
#   script:
#     - cargo tarpaulin --all
#   allow_failure: true

# For now, Clippy will just care about performance improvements as much of the
# codebase is not in a place that cleaning warnings will be benificial

clippy:
  stage: post-build
  image: registry.gitlab.com/veloren/veloren-docker-ci
  tags:
    - veloren-docker
  script:
    - cargo clippy -- -W clippy::all

.commit: &commit
  stage: post-build
  image: registry.gitlab.com/veloren/veloren-docker-ci
  tags:
    - veloren-docker
  except:
    - schedules
  only:
    refs:
      - master
  artifacts:
    paths:
      - ./
    expire_in: 1 week

commit:linux-debug:
  <<: *commit
  script:
    - (cd voxygen && VELOREN_ASSETS=assets cargo build)
    - (cd server-cli && VELOREN_ASSETS=assets cargo build)
    - cp target/debug/veloren-server-cli $CI_PROJECT_DIR
    - cp target/debug/veloren-voxygen $CI_PROJECT_DIR
    - cp -r assets $CI_PROJECT_DIR

commit:windows-debug:
  <<: *commit
  script:
    - (cd voxygen && VELOREN_ASSETS=assets cargo build --target=x86_64-pc-windows-gnu)
    - (cd server-cli && VELOREN_ASSETS=assets cargo build --target=x86_64-pc-windows-gnu)
    - cp target/x86_64-pc-windows-gnu/debug/veloren-server-cli.exe $CI_PROJECT_DIR
    - cp target/x86_64-pc-windows-gnu/debug/veloren-voxygen.exe $CI_PROJECT_DIR
    - cp -r assets $CI_PROJECT_DIR
